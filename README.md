# 99-Elixir-Problems

![](./assets/images.jpg)
## 99 Problems done purely in Elixir
---
- [x] Day 1 : Last element 
- [x] Day 2 : Second last element
- [x] Day 3 : Kth element
- [x] Day 4 : Reverse List
- [x] Day 5 : Palindrome check
- [x] Day 6 : Flatten List

## About Elixir
Elixir is a dynamic, functional language for building scalable and maintainable applications`


