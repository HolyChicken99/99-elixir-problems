defmodule Main do
  def fun(n) when n > 0, do: funp([1], n - 1)
  defp funp(a, n) when n > 0, do: funp([1|a ], n - 1)
  defp funp(a, n) when n == 0, do: a
end

IO.gets("")
|> String.trim()
|> String.to_integer()
|> Main.fun()
|> Enum.join(",")
|> IO.puts()
