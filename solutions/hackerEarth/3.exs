defmodule Main do
  def get_multiplication_factor() do
    IO.gets("")
    |> String.trim()
    |> String.to_integer()
  end

  def number_array(n) do
    :all
    |> IO.read()
    |> String.trim()
    |> String.split("\n")
    |> Enum.each(fn x -> for _i <- 1..n, do: IO.puts(x) end)
  end
end

Main.get_multiplication_factor() |> Main.number_array()

# https://www.hackerrank.com/challenges/fp-hello-world-n-times
