defmodule Main do
  def check?(a, b) do
    if a < b do
      IO.puts(a)
    else
      nil
    end
  end
end

num =
  IO.gets("")
  |> String.trim()
  |> String.to_integer()

:all
|> IO.read()
|> String.trim()
|> String.split("\n")
|> Enum.each(fn x -> Main.check?(String.to_integer(x), num) end)
