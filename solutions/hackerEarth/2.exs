defmodule Main do
  def fun() do
    IO.gets("") |> String.trim() |> String.to_integer()
  end

  def help(n) do
    if n == 0 do
      nil
    else
      IO.puts("Hello World")
      help(n - 1)
    end
  end
end

Main.fun() |> Main.help()
