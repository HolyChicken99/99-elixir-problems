require Integer

defmodule Main do
  def fun(arr) do
    for i <- 1..Enum.count(arr) do
      if(Integer.is_odd(i)) do
        IO.puts(Enum.at(arr, i))
      end
    end
  end
end

:all
|> IO.read()
|> String.trim()
|> String.split("\n")
|> Main.fun()
